% Simscape(TM) Multibody(TM) version: 7.2

% This is a model data file derived from a Simscape Multibody Import XML file using the smimport function.
% The data in this file sets the block parameter values in an imported Simscape Multibody model.
% For more information on this file, see the smimport function help page in the Simscape Multibody documentation.
% You can modify numerical values, but avoid any other changes to this file.
% Do not add code to this file. Do not edit the physical units shown in comments.

%%%VariableName:smiData


%============= RigidTransform =============%

%Initialize the RigidTransform structure array by filling in null values.
smiData.RigidTransform(23).translation = [0.0 0.0 0.0];
smiData.RigidTransform(23).angle = 0.0;
smiData.RigidTransform(23).axis = [0.0 0.0 0.0];
smiData.RigidTransform(23).ID = '';

%Translation Method - Cartesian
%Rotation Method - Arbitrary Axis
smiData.RigidTransform(1).translation = [30.627483539950767 -12.172352403788061 48.687134383378485];  % mm
smiData.RigidTransform(1).angle = 2.0943951023932117;  % rad
smiData.RigidTransform(1).axis = [-0.57735026918963117 -0.57735026918962307 -0.57735026918962307];
smiData.RigidTransform(1).ID = 'B[ENSAMBLAJE HOMBRO-1:-:EnsamblajeBrazo1_V6-1]';

%Translation Method - Cartesian
%Rotation Method - Arbitrary Axis
smiData.RigidTransform(2).translation = [12.95105312704856 26.06962535511639 158.54817430044281];  % mm
smiData.RigidTransform(2).angle = 2.0943951023931793;  % rad
smiData.RigidTransform(2).axis = [-0.57735026918962029 -0.57735026918962917 -0.57735026918962773];
smiData.RigidTransform(2).ID = 'F[ENSAMBLAJE HOMBRO-1:-:EnsamblajeBrazo1_V6-1]';

%Translation Method - Cartesian
%Rotation Method - Arbitrary Axis
smiData.RigidTransform(3).translation = [-155.04894687295115 61.06962535511687 158.54817430044309];  % mm
smiData.RigidTransform(3).angle = 2.0943951023931953;  % rad
smiData.RigidTransform(3).axis = [-0.57735026918962562 -0.57735026918962562 -0.57735026918962595];
smiData.RigidTransform(3).ID = 'B[EnsamblajeBrazo1_V6-1:-:brazo2_V4.5-1]';

%Translation Method - Cartesian
%Rotation Method - Arbitrary Axis
smiData.RigidTransform(4).translation = [144.50000000000009 -29.999999999999041 5.6843418860808015e-14];  % mm
smiData.RigidTransform(4).angle = 2.0943951023931953;  % rad
smiData.RigidTransform(4).axis = [-0.57735026918962584 -0.57735026918962584 -0.57735026918962584];
smiData.RigidTransform(4).ID = 'F[EnsamblajeBrazo1_V6-1:-:brazo2_V4.5-1]';

%Translation Method - Cartesian
%Rotation Method - Arbitrary Axis
smiData.RigidTransform(5).translation = [0 0 0];  % mm
smiData.RigidTransform(5).angle = 3.1415926535897931;  % rad
smiData.RigidTransform(5).axis = [1 0 0];
smiData.RigidTransform(5).ID = 'B[SOPORTE MOTOR Y RODAMIENTO (1)-1:-:motor eje z-1]';

%Translation Method - Cartesian
%Rotation Method - Arbitrary Axis
smiData.RigidTransform(6).translation = [-1.0964215646502851e-12 76.999999999999957 -5.6843418860808015e-14];  % mm
smiData.RigidTransform(6).angle = 1.5707963267948966;  % rad
smiData.RigidTransform(6).axis = [1 -5.5511151231257821e-17 -5.5511151231257827e-17];
smiData.RigidTransform(6).ID = 'F[SOPORTE MOTOR Y RODAMIENTO (1)-1:-:motor eje z-1]';

%Translation Method - Cartesian
%Rotation Method - Arbitrary Axis
smiData.RigidTransform(7).translation = [-16.734384161080555 25.000000000000021 224.71719206515803];  % mm
smiData.RigidTransform(7).angle = 2.0943951023931953;  % rad
smiData.RigidTransform(7).axis = [-0.57735026918962584 -0.57735026918962584 -0.57735026918962584];
smiData.RigidTransform(7).ID = 'B[PERFIL 50X50-1:-:SOPORTE MOTOR Y RODAMIENTO (1)-1]';

%Translation Method - Cartesian
%Rotation Method - Arbitrary Axis
smiData.RigidTransform(8).translation = [66.734384161081749 -41.59999998999848 48.282807934842026];  % mm
smiData.RigidTransform(8).angle = 2.0943951023931953;  % rad
smiData.RigidTransform(8).axis = [-0.57735026918962584 0.57735026918962584 0.57735026918962584];
smiData.RigidTransform(8).ID = 'F[PERFIL 50X50-1:-:SOPORTE MOTOR Y RODAMIENTO (1)-1]';

%Translation Method - Cartesian
%Rotation Method - Arbitrary Axis
smiData.RigidTransform(9).translation = [0 0 0];  % mm
smiData.RigidTransform(9).angle = 2.0943951023931953;  % rad
smiData.RigidTransform(9).axis = [-0.57735026918962584 -0.57735026918962584 -0.57735026918962584];
smiData.RigidTransform(9).ID = 'B[BASE CON EMPOTRAMIENTO-1:-:PERFIL 50X50-1]';

%Translation Method - Cartesian
%Rotation Method - Arbitrary Axis
smiData.RigidTransform(10).translation = [104.17721518987344 -32.259810126582181 365];  % mm
smiData.RigidTransform(10).angle = 3.1415926535897931;  % rad
smiData.RigidTransform(10).axis = [0.70710678118654757 0.70710678118654757 -5.5511151231257839e-17];
smiData.RigidTransform(10).ID = 'F[BASE CON EMPOTRAMIENTO-1:-:PERFIL 50X50-1]';

%Translation Method - Cartesian
%Rotation Method - Arbitrary Axis
smiData.RigidTransform(11).translation = [9.9999999999999947 0 0];  % mm
smiData.RigidTransform(11).angle = 2.0943951023931953;  % rad
smiData.RigidTransform(11).axis = [0.57735026918962584 0.57735026918962584 0.57735026918962584];
smiData.RigidTransform(11).ID = 'B[FA(S)MM-S20-1:-:HUSILLO-1]';

%Translation Method - Cartesian
%Rotation Method - Arbitrary Axis
smiData.RigidTransform(12).translation = [3.4229693382648698e-13 196.00000000000011 -1.0714830458236672e-12];  % mm
smiData.RigidTransform(12).angle = 2.0943951023931953;  % rad
smiData.RigidTransform(12).axis = [0.57735026918962584 -0.57735026918962584 0.57735026918962584];
smiData.RigidTransform(12).ID = 'F[FA(S)MM-S20-1:-:HUSILLO-1]';

%Translation Method - Cartesian
%Rotation Method - Arbitrary Axis
smiData.RigidTransform(13).translation = [0 102 7.0000000000000062];  % mm
smiData.RigidTransform(13).angle = 2.0943951023931953;  % rad
smiData.RigidTransform(13).axis = [0.57735026918962584 -0.57735026918962584 0.57735026918962584];
smiData.RigidTransform(13).ID = 'B[motor eje z-1:-:FA(S)MM-S20-1]';

%Translation Method - Cartesian
%Rotation Method - Arbitrary Axis
smiData.RigidTransform(14).translation = [1.4210854715202007e-14 -5.727973028106745e-15 -5.6678538004485226e-14];  % mm
smiData.RigidTransform(14).angle = 2.0943951023931953;  % rad
smiData.RigidTransform(14).axis = [-0.57735026918962573 -0.57735026918962584 0.57735026918962573];
smiData.RigidTransform(14).ID = 'F[motor eje z-1:-:FA(S)MM-S20-1]';

%Translation Method - Cartesian
%Rotation Method - Arbitrary Axis
smiData.RigidTransform(15).translation = [-2.3000000000000242 -250 6.2999999999999998];  % mm
smiData.RigidTransform(15).angle = 2.0943951023931953;  % rad
smiData.RigidTransform(15).axis = [-0.57735026918962584 -0.57735026918962584 -0.57735026918962584];
smiData.RigidTransform(15).ID = 'B[TS_04_09_500_1-1:-:ENSAMBLAJE HOMBRO-1]';

%Translation Method - Cartesian
%Rotation Method - Arbitrary Axis
smiData.RigidTransform(16).translation = [-42.792516460049882 -163.97981287116133 50.987134393378483];  % mm
smiData.RigidTransform(16).angle = 1.5707963267948966;  % rad
smiData.RigidTransform(16).axis = [-1 7.8504622934188758e-17 7.8504622934188758e-17];
smiData.RigidTransform(16).ID = 'F[TS_04_09_500_1-1:-:ENSAMBLAJE HOMBRO-1]';

%Translation Method - Cartesian
%Rotation Method - Arbitrary Axis
smiData.RigidTransform(17).translation = [25 -24.999999999999993 365];  % mm
smiData.RigidTransform(17).angle = 2.0943951023931953;  % rad
smiData.RigidTransform(17).axis = [0.57735026918962584 0.57735026918962584 0.57735026918962584];
smiData.RigidTransform(17).ID = 'B[PERFIL 50X50-1:-:TS_04_09_500_1-1]';

%Translation Method - Cartesian
%Rotation Method - Arbitrary Axis
smiData.RigidTransform(18).translation = [25.200000000000042 -290 3.5527136788005009e-15];  % mm
smiData.RigidTransform(18).angle = 3.1415926535897931;  % rad
smiData.RigidTransform(18).axis = [1.1102230246251568e-16 -6.1629758220391561e-33 -1];
smiData.RigidTransform(18).ID = 'F[PERFIL 50X50-1:-:TS_04_09_500_1-1]';

%Translation Method - Cartesian
%Rotation Method - Arbitrary Axis
smiData.RigidTransform(19).translation = [-24.372516460048214 44.577647596212024 108.18713438337852];  % mm
smiData.RigidTransform(19).angle = 1.5707963267949003;  % rad
smiData.RigidTransform(19).axis = [6.9829626776862415e-15 1 -6.9829626776862415e-15];
smiData.RigidTransform(19).ID = 'AssemblyGround[ENSAMBLAJE HOMBRO-1:screw_4-1]';

%Translation Method - Cartesian
%Rotation Method - Arbitrary Axis
smiData.RigidTransform(20).translation = [-43.992516460049004 75.813865940326323 48.687134393378514];  % mm
smiData.RigidTransform(20).angle = 1.5707963267949003;  % rad
smiData.RigidTransform(20).axis = [0 1 0];
smiData.RigidTransform(20).ID = 'AssemblyGround[ENSAMBLAJE HOMBRO-1:TW_04_09_3-1]';

%Translation Method - Cartesian
%Rotation Method - Arbitrary Axis
smiData.RigidTransform(21).translation = [-43.992516460049004 13.813865940326298 48.687134393378514];  % mm
smiData.RigidTransform(21).angle = 1.5707963267949003;  % rad
smiData.RigidTransform(21).axis = [0 1 0];
smiData.RigidTransform(21).ID = 'AssemblyGround[ENSAMBLAJE HOMBRO-1:TW_04_09_3-2]';

%Translation Method - Cartesian
%Rotation Method - Arbitrary Axis
smiData.RigidTransform(22).translation = [-39.372516460049233 -12.172352403787084 48.687134383378741];  % mm
smiData.RigidTransform(22).angle = 1.570796326794893;  % rad
smiData.RigidTransform(22).axis = [-1.3965925355372533e-14 -1 0];
smiData.RigidTransform(22).ID = 'AssemblyGround[ENSAMBLAJE HOMBRO-1:Base_v6-1]';

%Translation Method - Cartesian
%Rotation Method - Arbitrary Axis
smiData.RigidTransform(23).translation = [54.222821850532874 -8.8541477787654141 219.2078438912618];  % mm
smiData.RigidTransform(23).angle = 0;  % rad
smiData.RigidTransform(23).axis = [0 0 0];
smiData.RigidTransform(23).ID = 'RootGround[BASE CON EMPOTRAMIENTO-1]';


%============= Solid =============%
%Center of Mass (CoM) %Moments of Inertia (MoI) %Product of Inertia (PoI)

%Initialize the Solid structure array by filling in null values.
smiData.Solid(12).mass = 0.0;
smiData.Solid(12).CoM = [0.0 0.0 0.0];
smiData.Solid(12).MoI = [0.0 0.0 0.0];
smiData.Solid(12).PoI = [0.0 0.0 0.0];
smiData.Solid(12).color = [0.0 0.0 0.0];
smiData.Solid(12).opacity = 0.0;
smiData.Solid(12).ID = '';

%Inertia Type - Custom
%Visual Properties - Simple
smiData.Solid(1).mass = 0.52530104029023394;  % kg
smiData.Solid(1).CoM = [0 34.175489699910841 0.15871333383467312];  % mm
smiData.Solid(1).MoI = [269.8950744900896 84.275747358629687 269.4235679766104];  % kg*mm^2
smiData.Solid(1).PoI = [-4.2667281524122664 0 0];  % kg*mm^2
smiData.Solid(1).color = [0.6470588235294118 0.61960784313725492 0.58823529411764708];
smiData.Solid(1).opacity = 1;
smiData.Solid(1).ID = 'motor eje z*:*Predeterminado';

%Inertia Type - Custom
%Visual Properties - Simple
smiData.Solid(2).mass = 0;  % kg
smiData.Solid(2).CoM = [0 0 0];  % mm
smiData.Solid(2).MoI = [0 0 0];  % kg*mm^2
smiData.Solid(2).PoI = [0 0 0];  % kg*mm^2
smiData.Solid(2).color = [0.89803921568627454 0.91764705882352937 0.92941176470588238];
smiData.Solid(2).opacity = 1;
smiData.Solid(2).ID = 'FA(S)MM-S20*:*Predeterminado';

%Inertia Type - Custom
%Visual Properties - Simple
smiData.Solid(3).mass = 0.0074137755870066577;  % kg
smiData.Solid(3).CoM = [-0.0046594921595204709 6.5958946821748956 0];  % mm
smiData.Solid(3).MoI = [0.40066773517576215 0.65745717601004239 0.59581572738921751];  % kg*mm^2
smiData.Solid(3).PoI = [0 0 -0.00022219327772280797];  % kg*mm^2
smiData.Solid(3).color = [0.44313725490196076 0.078431372549019607 0];
smiData.Solid(3).opacity = 1;
smiData.Solid(3).ID = 'screw_4*:*Predeterminado';

%Inertia Type - Custom
%Visual Properties - Simple
smiData.Solid(4).mass = 0.0030787879376401885;  % kg
smiData.Solid(4).CoM = [6.3707762710574499e-05 14.499861167443086 1.2421563296435214];  % mm
smiData.Solid(4).MoI = [0.21976076795225261 0.13547216704468851 0.31970019183875809];  % kg*mm^2
smiData.Solid(4).PoI = [1.5093581666259677e-06 5.0234613009617041e-07 2.3569022542662779e-07];  % kg*mm^2
smiData.Solid(4).color = [0.93725490196078431 0.83921568627450982 0.6705882352941176];
smiData.Solid(4).opacity = 1;
smiData.Solid(4).ID = 'TW_04_09_3*:*Predeterminado';

%Inertia Type - Custom
%Visual Properties - Simple
smiData.Solid(5).mass = 0.45880985391707141;  % kg
smiData.Solid(5).CoM = [3.074364639424588 67.874804737118652 -37.294700431049058];  % mm
smiData.Solid(5).MoI = [1362.1184389463933 595.477550772977 1240.1063992001664];  % kg*mm^2
smiData.Solid(5).PoI = [41.461227973640938 -32.953578834901116 -2.7110464543464694];  % kg*mm^2
smiData.Solid(5).color = [0.29803921568627451 0.29803921568627451 0.29803921568627451];
smiData.Solid(5).opacity = 1;
smiData.Solid(5).ID = 'Base_v6*:*Predeterminado';

%Inertia Type - Custom
%Visual Properties - Simple
smiData.Solid(6).mass = 0.55395139834468043;  % kg
smiData.Solid(6).CoM = [-0.022759740192051684 -0.0098320934169916965 182.29870839879635];  % mm
smiData.Solid(6).MoI = [6355.0832354527774 6354.7361435763569 409.84401902677831];  % kg*mm^2
smiData.Solid(6).PoI = [0.37050562519629904 -0.32632230146033314 0.0095172807439462773];  % kg*mm^2
smiData.Solid(6).color = [0.89803921568627454 0.91764705882352937 0.92941176470588238];
smiData.Solid(6).opacity = 1;
smiData.Solid(6).ID = 'PERFIL 50X50*:*Predeterminado';

%Inertia Type - Custom
%Visual Properties - Simple
smiData.Solid(7).mass = 0.1299273164130515;  % kg
smiData.Solid(7).CoM = [107.82182750464844 0.1433982718418669 4.7924775869804907e-06];  % mm
smiData.Solid(7).MoI = [46.297924751194486 180.59173944313909 178.12844143808474];  % kg*mm^2
smiData.Solid(7).PoI = [-1.3189462913906485e-06 -5.2280682105805331e-05 1.6112788947588588];  % kg*mm^2
smiData.Solid(7).color = [1 1 1];
smiData.Solid(7).opacity = 1;
smiData.Solid(7).ID = 'brazo2_V4.5*:*Predeterminado';

%Inertia Type - Custom
%Visual Properties - Simple
smiData.Solid(8).mass = 0.17537633349540649;  % kg
smiData.Solid(8).CoM = [45.735552691566127 -47.597935817625498 13.295621563106607];  % mm
smiData.Solid(8).MoI = [279.90149425233062 206.36246531087178 296.59089306503625];  % kg*mm^2
smiData.Solid(8).PoI = [27.577463817792598 -13.651586160867687 66.146151682449386];  % kg*mm^2
smiData.Solid(8).color = [1 1 1];
smiData.Solid(8).opacity = 1;
smiData.Solid(8).ID = 'SOPORTE MOTOR Y RODAMIENTO (1)*:*Predeterminado';

%Inertia Type - Custom
%Visual Properties - Simple
smiData.Solid(9).mass = 0.91086521778612861;  % kg
smiData.Solid(9).CoM = [-83.467467452169728 7.1788924455339762 32.159969298617071];  % mm
smiData.Solid(9).MoI = [2401.3893964362574 5031.7827486383467 2824.2622582556942];  % kg*mm^2
smiData.Solid(9).PoI = [-0.07882785811387788 -0.050642894656855598 88.988743076317931];  % kg*mm^2
smiData.Solid(9).color = [0.25098039215686274 0.25098039215686274 0.25098039215686274];
smiData.Solid(9).opacity = 1;
smiData.Solid(9).ID = 'BASE CON EMPOTRAMIENTO*:*Predeterminado';

%Inertia Type - Custom
%Visual Properties - Simple
smiData.Solid(10).mass = 0.10424679497406082;  % kg
smiData.Solid(10).CoM = [5.5114560473083695e-10 -87.36279169939516 3.4227531098016222];  % mm
smiData.Solid(10).MoI = [919.68220011304913 1.0424556264772664 919.90984792237884];  % kg*mm^2
smiData.Solid(10).PoI = [-0.0099065700051706226 0 0];  % kg*mm^2
smiData.Solid(10).color = [0.6470588235294118 0.61960784313725492 0.58823529411764708];
smiData.Solid(10).opacity = 1;
smiData.Solid(10).ID = 'TS_04_09_500_1*:*Predeterminado';

%Inertia Type - Custom
%Visual Properties - Simple
smiData.Solid(11).mass = 0.44690166438216405;  % kg
smiData.Solid(11).CoM = [-46.772793718149629 91.752440583870111 158.54858314450169];  % mm
smiData.Solid(11).MoI = [512.79241958122395 1250.9915391410927 1385.0397383742336];  % kg*mm^2
smiData.Solid(11).PoI = [-0.0084317814200760895 0.0055436009788622127 29.897109153577428];  % kg*mm^2
smiData.Solid(11).color = [1 1 1];
smiData.Solid(11).opacity = 1;
smiData.Solid(11).ID = 'EnsamblajeBrazo1_V6*:*Predeterminado';

%Inertia Type - Custom
%Visual Properties - Simple
smiData.Solid(12).mass = 0.079394329541521258;  % kg
smiData.Solid(12).CoM = [0 93.037037037037038 0];  % mm
smiData.Solid(12).MoI = [265.34750725450277 0.60379840005669039 265.34750725450277];  % kg*mm^2
smiData.Solid(12).PoI = [0 0 0];  % kg*mm^2
smiData.Solid(12).color = [0.6470588235294118 0.61960784313725492 0.58823529411764708];
smiData.Solid(12).opacity = 1;
smiData.Solid(12).ID = 'HUSILLO*:*Default';


%============= Joint =============%
%X Revolute Primitive (Rx) %Y Revolute Primitive (Ry) %Z Revolute Primitive (Rz)
%X Prismatic Primitive (Px) %Y Prismatic Primitive (Py) %Z Prismatic Primitive (Pz) %Spherical Primitive (S)
%Constant Velocity Primitive (CV) %Lead Screw Primitive (LS)
%Position Target (Pos)

%Initialize the PrismaticJoint structure array by filling in null values.
smiData.PrismaticJoint(1).Pz.Pos = 0.0;
smiData.PrismaticJoint(1).ID = '';

smiData.PrismaticJoint(1).Pz.Pos = 0;  % m
smiData.PrismaticJoint(1).ID = '[TS_04_09_500_1-1:-:ENSAMBLAJE HOMBRO-1]';


%Initialize the RevoluteJoint structure array by filling in null values.
smiData.RevoluteJoint(4).Rz.Pos = 0.0;
smiData.RevoluteJoint(4).ID = '';

smiData.RevoluteJoint(1).Rz.Pos = -174.07522430061417;  % deg
smiData.RevoluteJoint(1).ID = '[ENSAMBLAJE HOMBRO-1:-:EnsamblajeBrazo1_V6-1]';

smiData.RevoluteJoint(2).Rz.Pos = -67.437055225501695;  % deg
smiData.RevoluteJoint(2).ID = '[EnsamblajeBrazo1_V6-1:-:brazo2_V4.5-1]';

smiData.RevoluteJoint(3).Rz.Pos = 163.18760429918183;  % deg
smiData.RevoluteJoint(3).ID = '[FA(S)MM-S20-1:-:HUSILLO-1]';

smiData.RevoluteJoint(4).Rz.Pos = -91.99242991503769;  % deg
smiData.RevoluteJoint(4).ID = '[motor eje z-1:-:FA(S)MM-S20-1]';

